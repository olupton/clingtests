#pragma once
#include <map>
#include <string>
#include <memory>
#include <string_view>
#include <functional>

template <typename F>
struct Impl {
  Impl( F f ) : m_f{std::move( f )} {}
  auto operator()() const {
    auto result = 1.f;
    for ( auto i = 0; i < 5; ++i ) {
      result *= 2.f * std::invoke( m_f );
    }
    return result;
  }
private:
  F m_f;
};

template <typename F>
auto Wrapper( F f ) {
  return Impl<F>( std::move( f ) );
}

template <typename T>
struct Number {
  Number( T x ) : m_x{x} {}
  T operator()() const { return m_x; }
private:
  T m_x;
};

template <typename T>
auto make( T x ) {
  return Wrapper( Wrapper( Wrapper( Wrapper( Wrapper( Wrapper( Wrapper( Number<T>( x ) ) ) ) ) ) ) );
}

struct AnyFunctor {
  virtual ~AnyFunctor() = default;
};

struct Registry {
  static Registry& instance() {
    static Registry r;
    return r;
  }
  void add( std::string key, std::function<std::unique_ptr<AnyFunctor>()> factory ) {
    m_factories.emplace( std::move( key ), std::move( factory ) ); 
  }
  auto get( std::string_view key ) const {
    auto iter = m_factories.find( key );
    if ( iter == m_factories.end() ) {
      throw std::runtime_error( "Unknown key: " + std::string{key} );
    }
    return std::invoke( iter->second );
  }
private:
  std::map<std::string, std::function<std::unique_ptr<AnyFunctor>()>, std::less<>> m_factories;
};

struct Declare {
  Declare( std::string key, std::function<std::unique_ptr<AnyFunctor>()> val ) {
    Registry::instance().add( std::move( key ), std::move( val ) );
  }
};

template <typename> struct Functor;

template <typename result_type, typename... InputType>
class Functor<result_type( InputType... )> final : public AnyFunctor {
  struct IFunctor {
    virtual ~IFunctor()                                  = default;
    virtual result_type operator()( InputType... ) const = 0;
  };

  template <typename F>
  struct FunctorImpl : public IFunctor {
    FunctorImpl( F f ) : m_f( std::move( f ) ) {}
    result_type operator()( InputType... input ) const override {
      return std::invoke( m_f, input... );
    }
  private:
    F m_f;
  };
  std::unique_ptr<IFunctor> m_functor;
    
public:
  Functor() = default;
  Functor( Functor&& other ) = default;
  Functor& operator=( Functor&& other ) = default;

  template <typename F>
  Functor( F func ) : m_functor( std::make_unique<FunctorImpl<F>>( std::move( func ) ) ) {}

  template <typename F>
  Functor& operator=( F func ) {
    m_functor = std::make_unique<FunctorImpl<F>>( std::move( func ) );
    return *this;
  }

  result_type operator()( InputType... input ) const {
    return std::invoke( *m_functor, input... );
  }
};