#include "test.h"
namespace {
  Declare foo{"lib_make", []() -> std::unique_ptr<AnyFunctor> { return std::make_unique<Functor<float()>>( make( 1.f ) ); }};
}