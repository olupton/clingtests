#!/bin/bash
set -e
. /cvmfs/sft.cern.ch/lcg/views/LCG_96b/x86_64-centos7-gcc9-opt/setup.sh
# . /cvmfs/sft-nightlies.cern.ch/lcg/views/dev3/Tue/x86_64-centos7-gcc9-opt/setup.sh
CC=g++
CCLIB=g++
BUILDPREFIX=""
# Uncomment next two lines to build with clang-5
CCLIB=clang++
BUILDPREFIX=". /cvmfs/sft.cern.ch/lcg/contrib/llvm/5.0/x86_64-centos7/setup.sh && "
BUILDLIB="${CCLIB} `root-config --cflags` -shared -o libtest.so lib.cpp -O3 -fPIC"
sh -c "${BUILDPREFIX}${BUILDLIB}"
$CC `root-config --cflags --ldflags --libs` -DTEST0 -o test0 test.cpp
$CC `root-config --cflags --ldflags --libs` -DTEST1 -o test1 test.cpp
$CC `root-config --cflags --ldflags --libs` -DTEST2 -o test2 test.cpp
$CC `root-config --cflags --ldflags --libs` -DTEST3 -o test3 test.cpp
$CC `root-config --cflags --ldflags --libs` -DTEST4 -o test4 test.cpp
$CC `root-config --cflags --ldflags --libs` -DTEST5 -o test5 test.cpp
root-config --version
./test0
./test1
./test2
./test3
./test4
./test5
