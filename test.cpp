#include <TInterpreter.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <x86intrin.h>
#include <functional>
#include "test.h"
#include <dlfcn.h>

template <typename F>
auto measure( F&& f ) {
  // Warmup
  for ( auto i = 0ul; i < 50; ++i ) {
    f();
  }
  _mm_lfence();
  auto const start = __rdtsc();
  _mm_lfence();
  for ( auto i = 0ul; i < 100; ++i ) {
    f();
    _mm_lfence();
  }
  _mm_lfence();
  auto const ticks = __rdtsc() - start;
  _mm_lfence();
  return ticks;
}

using any_ptr  = std::unique_ptr<AnyFunctor>;

template <typename RType>
Functor<RType()> use_cling( std::string_view name, std::string_view argument, int opt_level = 3 ) {
  auto RType_str = [](){
    if constexpr ( std::is_same_v<RType, float> ) {
      return "float";
    } else if constexpr ( std::is_same_v<RType, double> ) {
      return "double";
    }
  }();
  std::ostringstream code;
  code << "#pragma cling optimize(" << opt_level << ")\n";
  code << "#include \"test.h\"\n";
  code << "std::unique_ptr<AnyFunctor> " << name << "() { return std::make_unique<Functor<" << RType_str << "()>>( make( " << argument << " ) ); }\n";
  code << "auto const " << name << "_addr = " << name << ";\n";
  gInterpreter->Declare( code.str().c_str() );
  using make_any_ptr = any_ptr ( * )();
  auto factory  = reinterpret_cast<make_any_ptr>( gInterpreter->Calc( ( std::string{name} + "_addr" ).c_str() ) );
  auto base_ptr = factory();
  return std::move( *dynamic_cast<Functor<RType()>*>( base_ptr.get() ) );
}

template <typename RType = float>
Functor<RType()> use_dlopen() {
  using func_t = Functor<RType()>;
  any_ptr lib_make_ptr = Registry::instance().get( "lib_make" );
  return std::move( *dynamic_cast<Functor<RType()>*>( lib_make_ptr.get() ) );
}

void test0() {
  std::cout << "test0 -- only cling, no optimisation, all similar speed" << std::endl;
  std::cout << "cling_flt_flt " << measure(use_cling<float>("cling_flt_flt", "1.f", 0)) << " ticks" << std::endl;
  std::cout << "cling_flt_dbl " << measure(use_cling<float>("cling_flt_dbl", "1.", 0)) << " ticks" << std::endl;
  std::cout << "cling_dbl_dbl " << measure(use_cling<double>("cling_dbl_dbl", "1.", 0)) << " ticks" << std::endl;
}

// Try various combinations with cling, no library loading
void test1() {
  std::cout << "test1 -- only cling, all similar speed" << std::endl;
  std::cout << "cling_flt_flt " << measure(use_cling<float>("cling_flt_flt", "1.f")) << " ticks" << std::endl;
  std::cout << "cling_flt_dbl " << measure(use_cling<float>("cling_flt_dbl", "1.")) << " ticks" << std::endl;
  std::cout << "cling_dbl_dbl " << measure(use_cling<double>("cling_dbl_dbl", "1.")) << " ticks" << std::endl;
}

// Second test, show that loading the library affects cling
void test2(bool skip_header = false) {
  if ( !skip_header ) {
    std::cout << "test2 -- loading library, cling is fast when the signature matches exactly (cling_flt_flt)" << std::endl;
  }
  dlopen("./libtest.so", RTLD_LAZY | RTLD_GLOBAL);
  std::cout << "dlopenlibrary " << measure(use_dlopen()) << " ticks" << std::endl;
  std::cout << "cling_flt_1.f " << measure(use_cling<float>("cling_flt_flt1", "1.f")) << " ticks" << std::endl;
  std::cout << "cling_flt_2.f " << measure(use_cling<float>("cling_flt_flt2", "2.f")) << " ticks" << std::endl;
  std::cout << "cling_flt_dbl " << measure(use_cling<float>("cling_flt_dbl", "1.")) << " ticks" << std::endl;
  std::cout << "cling_dbl_dbl " << measure(use_cling<double>("cling_dbl_dbl", "1.")) << " ticks" << std::endl;
}

// Third test, show that unloading the library works
void test3() {
  std::cout << "test3 -- load the library but unload it afterwards, cling is always slow" << std::endl;
  auto handle = dlopen("./libtest.so", RTLD_LAZY | RTLD_GLOBAL);
  std::cout << "dlopenlibrary " << measure(use_dlopen()) << " ticks" << std::endl;
  dlclose( handle );
  std::cout << "cling_flt_1.f " << measure(use_cling<float>("cling_flt_flt1", "1.f")) << " ticks" << std::endl;
  std::cout << "cling_flt_2.f " << measure(use_cling<float>("cling_flt_flt2", "2.f")) << " ticks" << std::endl;
  std::cout << "cling_flt_dbl " << measure(use_cling<float>("cling_flt_dbl", "1.")) << " ticks" << std::endl;
  std::cout << "cling_dbl_dbl " << measure(use_cling<double>("cling_dbl_dbl", "1.")) << " ticks" << std::endl;
}

// Fourth test, show that using cling before opening the library changes the behaviour
void test4() {
  std::cout << "test4 -- same as test2 but invoking cling once before loading the library with a signature matching the library" << std::endl;
  std::cout << "cling_flt_flt " << measure(use_cling<float>("cling_flt_flt1PRE", "1.f")) << " ticks" << std::endl;
  test2(true);
}

// Fifth test, show that using cling before opening the library doesn't changes the behaviour if the type didn't match
void test5() {
  std::cout << "test5 -- same as test5 but the initial cling invocation uses a signature different to that in the library" << std::endl;
  std::cout << "cling_dbl_flt " << measure(use_cling<double>("cling_dbl_flt1PRE", "1.f")) << " ticks" << std::endl;
  test2(true);
}

int main() {
#ifdef TEST0
  test0();
#elif TEST1
  test1();
#elif TEST2
  test2();
#elif TEST3
  test3();
#elif TEST4
  test4();
#else
  test5();
#endif
  return 0;
}